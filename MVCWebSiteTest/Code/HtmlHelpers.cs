﻿using DataAccess.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MVCWebSiteTest.Helpers
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString BootstrapPager(this HtmlHelper html, int currentPage, int totalPages, Func<int, string> getPageUrl, int prePagesQuantity = 3, int afterPagesQuantity = 3, int middlePagesQuantity = 2)
        {
            StringBuilder sb = new StringBuilder();

            // Prev link
            if (currentPage == 1)
            {
                var prevBuilder = new TagBuilder("span") { InnerHtml = "Prev" };
                prevBuilder.AddCssClass("page-link");
                sb.AppendLine("<li class='page-item page-prev disabled'>" + prevBuilder.ToString() + "</li>");
            }
            else
            {
                var prevBuilder = new TagBuilder("a");
                prevBuilder.MergeAttribute("href", getPageUrl.Invoke(currentPage - 1));
                prevBuilder.InnerHtml = "Prev";
                prevBuilder.AddCssClass("page-link");
                sb.AppendLine("<li class = 'page-item page-next'>" + prevBuilder.ToString() + "</li>");
            }

            // Pages
            for (int i = 1; i <= totalPages; i++)
            {
                //if (((i <= 3) || (i > (totalPages - 3))) || // first and last 3 pages or
                //  ((i > (currentPage - 2)) && (i < (currentPage + 2)))) // four near pages (2 before and 2 after current page)
                if (((i <= prePagesQuantity) || (i > (totalPages - afterPagesQuantity))) || // first and last 3 pages or
                ((i > (currentPage - middlePagesQuantity)) && (i < (currentPage + middlePagesQuantity)))) // four near pages (2 before and 2 after current page)
                {

                    if (i == currentPage)
                    {
                        var subBuilder = new TagBuilder("span") { InnerHtml = i.ToString() };
                        subBuilder.AddCssClass("page-link");
                        sb.AppendLine("<li class='page-item active'>" + subBuilder.ToString() + "</li>");
                    }
                    else
                    {
                        var subBuilder = new TagBuilder("a") { InnerHtml = i.ToString() };
                        subBuilder.AddCssClass("page-link");
                        subBuilder.MergeAttribute("href", getPageUrl.Invoke(i));
                        sb.AppendLine("<li class = 'page-item'>" + subBuilder.ToString() + "</li>");
                    }
                }
                //else if (((i == 4) && (currentPage > 5)) || // "..." beetween pages
                //((i == totalPages - 3) && (currentPage < (totalPages - 4))))
                else if (((i == prePagesQuantity + 1) && (currentPage > 5)) || // "..." beetween pages
                ((i == totalPages - afterPagesQuantity) && (currentPage < (totalPages - 4))))
                {
                    sb.AppendLine("<li class='page-item disabled'><span>...</span></li>");
                }
            }


            // Next link
            if (currentPage == totalPages || totalPages == 0)
            {
                var nextBuilder = new TagBuilder("span") { InnerHtml = "Next" };
                nextBuilder.AddCssClass("page-link");
                sb.AppendLine("<li class='page-item page-next disabled'>" + nextBuilder.ToString() + "</li>");
            }
            else
            {
                var nextBuilder = new TagBuilder("a") { InnerHtml = "Next" };
                nextBuilder.AddCssClass("page-link");
                nextBuilder.MergeAttribute("href", getPageUrl.Invoke(currentPage + 1));
                sb.AppendLine("<li  class='page-item page-next'>" + nextBuilder.ToString() + "</li>");
            }

            return new MvcHtmlString(sb.ToString());
        }

        /// <summary>
        /// Add activeClassName CSS class for HTML element if it's element is active
        /// </summary>
        /// <param name="html">HtmlHelper</param>
        /// <param name="action">Route Action</param>
        /// <param name="controller">Route Controller</param>
        /// <param name="activeClassName">Active CSS class name</param>
        /// <returns></returns>
        public static bool IsActive(this HtmlHelper html, string action = null, string controller = null)
        {
            string currentAction = html.ViewContext.RouteData.Values["action"] as string;
            string currentController = html.ViewContext.RouteData.Values["controller"] as string;

            if (String.IsNullOrEmpty(controller))
                controller = currentController;

            if (String.IsNullOrEmpty(action))
                action = currentAction;

            return (controller.Equals(currentController, StringComparison.InvariantCultureIgnoreCase)
                     && action.Equals(currentAction, StringComparison.InvariantCultureIgnoreCase));
        }
        public static bool IsAdmin(this HtmlHelper html, string userName)
        {
            bool isAdmin = false;
            string roleNameAdmin = "Admin";
            UserManager userMgr = HttpContext.Current
                .GetOwinContext().GetUserManager<UserManager>();
            
            if (userMgr.IsInRole(userMgr.FindByName(userName).Id, roleNameAdmin))
            {
                isAdmin = true;
            }
            return isAdmin;
        }
    }
}