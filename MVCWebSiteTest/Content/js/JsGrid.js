﻿var MyDateField = function (config) {
    jsGrid.Field.call(this, config);
};

MyDateField.prototype = new jsGrid.Field({

    css: "date-field",            // redefine general property 'css'
    align: "center",              // redefine general property 'align'

    myCustomProperty: "foo",      // custom property

    sorter: function (date1, date2) {
        return new Date(date1) - new Date(date2);
    },

    itemTemplate: function (value) {
        return new Date(new Date(value)).toLocaleDateString();
    },

    insertTemplate: function (value) {
        return this._insertPicker = $("<input>").datepicker({ defaultDate: new Date() });
    },

    editTemplate: function (value) {
        return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
    },

    insertValue: function () {
        return this._insertPicker.datepicker("getDate").toISOString();
    },

    editValue: function () {
        return this._editPicker.datepicker("getDate").toISOString();
    }
});

jsGrid.fields.date = MyDateField;

$("#jsGrid").jsGrid({
    width: "100%",
    //height: "400px",

    editing: true,
    sorting: true,
    autoheight: true,
    paging: true,
    pageLoading: true,
    autoload: true,

    pageSize: 3,
    pageButtonCount: 5,


    controller: {
        loadData: function (filter) {
            return $.ajax({
                type: "GET",
                url: "/AdminPanel/GetUsers",
                data: filter,
                dataType: "json"
            });
        },
        updateItem: function (item) {
            return $.ajax({
                type: "POST",
                url: "/AdminPanel/UpdateUser",
                data: item,
                dataType: "json"
            });
        },
        deleteItem: function (item) {
            return $.ajax({
                type: "POST",
                url: "/AdminPanel/DeleteUser",
                data: item,
                dataType: "json"
            });
        }
    },

    fields: [
        { name: "Id", type: "text", width: 200, validate: "required", readOnly: true },
        { name: "UserName", title: "Login", type: "text", validate: "required", readOnly: true, width: 100 },
        { name: "FirstName", title: "First Name", type: "text", width: 100 },
        { name: "LastName", title: "Last Name", type: "text", width: 100 },
        { name: "Age", title: "Age", type: "text", width: 100 },
        { name: "RegDate", title: "RegDate", type: "date", width: 100 },
        { name: "StudyDate", title: "StudyDate", type: "date", width: 100 },
        { name: "Email", title: "Email", type: "text", width: 100, readOnly: true },
        { name: "IsAdmin", title: "Is Admin", type: "checkbox", width: 100 },
        { type: "control" }
    ]
});

$("#jsGridRoles").jsGrid({
    width: "100%",

    autoheight: true,
    paging: true,
    pageLoading: true,
    autoload: true,

    pageSize: 3,
    pageButtonCount: 5,


    controller: {
        loadData: function (filter) {
            return $.ajax({
                type: "GET",
                url: "/AdminPanel/GetUserRoles",
                data: filter,
                dataType: "json"
            });
        },
        updateItem: function (item) {
            return $.ajax({
                type: "POST",
                url: "/AdminPanel/UpdateRole",
                data: item,
                dataType: "json"
            });
        },
        deleteItem: function (item) {
            return $.ajax({
                type: "POST",
                url: "/AdminPanel/DeleteRole",
                data: item,
                dataType: "json"
            });
        }
    },

    fields: [
        { name: "Id", type: "text", width: 200, validate: "required", readOnly: true },
        { name: "Name", title: "Role Name", type: "text", validate: "required", readOnly: true, width: 300 },
        //{ name: "Users", title: "User In Roles", type: "select", validate: "required", readOnly: true, width: 200 },
        //{ type: "control" }
    ]
});