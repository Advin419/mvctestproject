﻿$(function () {
    $('#Mydatepicker').datepicker({
        todayBtn: "linked",
        autoclose: true,
    }).on("changeDate", function (dateText) {
        setStudyDate(dateText)
    });
});
function setStudyDate(dateText) {
    var studyDate = dateText.date.toLocaleDateString();
    var userId = $('#idStudent').val();
    $('#sDateStudent').html(studyDate);
    var data = new FormData();
    data.append("StudyDate", studyDate);
    data.append("userId", userId);
    var location = window.location.origin;
    var url = location + "/Home/SetStudyDate/";
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        success: function (result) {
        },
        error: function (e) {
            alert('Sorry, it seems that our mail server is not responding. Please try again later!');
        },
    });
}
$('#editInfo').click(function () {
    setEditTable();
});
function setEditTable() {
    var table = $("#tableInfo table tbody");
    table.find('tr').each(function (key, val) {
        var thisNow = false;
        $(this).find('td.now').each(function (key, val) {
            thisNow = true;
            $(this).html($('.now input').val());
            $(this).removeClass('now');
            $(this).addClass('edit');
            $('#butSaveInfo').css('visibility', 'hidden');
        });
        if (!thisNow) {
            $(this).find('td.edit').each(function (key, val) {
                thisNow = false;
                $(this).removeClass('edit');
                $(this).addClass('now');
                if ($(this).attr('id') == 'rDateStudent' || $(this).attr('id') == 'sDateStudent') {
                    $(this).html('<input readonly class="form-control col-sm-12" id="editbox" size="' + $(this).text().length + '" type="text" value="' + $(this).text() + '" />');
                }
                else {
                    $(this).html('<input class="form-control col-sm-12" id="editbox" size="' + $(this).text().length + '" type="text" value="' + $(this).text() + '" />');
                }
                $('#butSaveInfo').css('visibility', 'visible');
            });
        }
    });
}
$('#butSaveInfo').click(function () {
    var location = window.location.origin;
    var url = location + "/Home/EditStudentInfo/";
    sendAjaxFormPost(url);
});
function reloadUserName() {
    var location = window.location.origin;
    var url = location + "/Home/UserLogin/";
    $.ajax({
        url: url,
        type: "GET",
        processData: false,
        contentType: false,
        cache: false,
        beforeSend: function () { $("#btnchatsubmit").attr('disabled', 'disabled'); },
        success: function (result) {
            if (result != null) {
                $('#userInfo').html();
                $('#userInfo').html(result);
            }
            else {
            }
        },
        error: function (e) {
            alert('Sorry, it seems that our mail server is not responding. Please try again later!');
        },
    }).always(function () { $("#btnchatsubmit").removeAttr('disabled'); });
}
function sendAjaxFormPost(url) {
    var data = new FormData();
    data = getDataTable();
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        beforeSend: function () { $("#btnchatsubmit").attr('disabled', 'disabled'); },
        success: function (result) {
            if (result != null) {
                setEditTable();
                reloadUserName();
                if (result.status == "Redirect") {
                    $('#textFormError').html();
                    $('#textFormError').html("Message was sent to the email with instructions for confirm.<br> Log in to the specified email and follow the link for confirmation.");
                    $('#textFormError').show();
                }
            }
            else {
                setEditTable();
            }
        },
        error: function (e) {
            alert('Sorry, it seems that our mail server is not responding. Please try again later!');
        },
    }).always(function () { $("#btnchatsubmit").removeAttr('disabled'); });
}
function getDataTable() {
    var data = new FormData();
    var Id = $('#idStudent').val();
    var UserName = $('#loginStudent input').val();
    var FirstName = $('#fNameStudent input').val()
    var LastName = $('#lNameStudent input').val();
    var Age = $('#ageStudent input').val();
    var RegDate = $('#rDateStudent input').val();
    var StudyDate = $('#sDateStudent input').val();
    var Email = $('#emailStudent input').val();
    data.append("Id", Id);
    data.append("UserName", UserName);
    data.append("FirstName", FirstName);
    data.append("LastName", LastName);
    data.append("Age", Age);
    data.append("RegDate", RegDate);
    data.append("StudyDate", StudyDate);
    data.append("Email", Email);
    return data;
}