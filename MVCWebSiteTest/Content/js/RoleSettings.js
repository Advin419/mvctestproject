﻿$('#butCreateRole').click(function () {
    var location = window.location.origin;
    var url = location + "/AdminPanel/CreateRole/";
    sendRoleName(url);
});
function sendRoleName(url) {
    var data = new FormData();
    var roleName = $('#roleName').val();
    data.append("roleName", roleName);
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        beforeSend: function () { $("#btnchatsubmit").attr('disabled', 'disabled'); },
        success: function (result) {
            if (result != null) {
                $('#roleName').val("");
                $("#jsGridRoles").jsGrid("loadData");
            }
            else {
                setEditTable();
            }
        },
        error: function (e) {
            alert('Sorry, it seems that our mail server is not responding. Please try again later!');
        },
    }).always(function () { $("#btnchatsubmit").removeAttr('disabled'); });
}