﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MVCWebSiteTest
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: RouteNames.AdminPanel,
                url: "AdminPanel/Index",
                defaults: new { controller = "AdminPanel", action = "Index" }
            );
            routes.MapRoute(
                name: RouteNames.AdminPanelPage,
                url: "AdminPanel/Index/page{page}",
                defaults: new { controller = "AdminPanel", action = "Index", page = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
    public static class RouteNames
    {
        public static string Login { get { return "Login"; } }
        public static string Register { get { return "Register"; } }
        public static string ForgotPassword { get { return "ForgotPassword"; } }
        public static string SignOut { get { return "SignOut"; } }
        public static string AdminPanelPage { get { return "AdminPanelPage"; } }
        public static string AdminPanel { get { return "AdminPanel"; } }
        public static string EditUser { get { return "EditUser"; } }
        public static string MessagePartial { get { return "MessagePartial"; } }
    }
}
