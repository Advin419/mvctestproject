﻿using DataAccess.Repository;
using Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MVCWebSiteTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVCWebSiteTest.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public async Task<ActionResult> Index()
        {
            User _user = await UserManager.FindByNameAsync(User.Identity.Name);
            UserProfile user = new UserProfile(_user);
            return View("StudentCabinet", model: user);
        }
        [Authorize]
        public async Task<ActionResult> StudentCabinet()
        {
            User _user = await UserManager.FindByNameAsync(User.Identity.Name);
            UserProfile user = new UserProfile(_user);
            return View(model:user);
        }
        [Authorize]
        public JsonResult SetStudyDate(string studyDate, string userId)
        {
            string status = "Error";
            User user = UserManager.FindById(userId);
            if (user != null)
            {
                user.StudyDate = Convert.ToDateTime(studyDate);
                IdentityResult result = UserManager.Update(user);
                if (result.Succeeded)
                {
                    status = "Succeeded";
                }
            }
            return Json(new { status = status }, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public async Task<JsonResult> EditStudentInfo(EditViewModel model) 
        {
            if (ModelState.IsValid)
            {
                bool emailUpdate = false;
                bool loginUpdate = false;
                User user = await UserManager.FindByIdAsync(model.Id);
                if (user != null)
                {
                    if (model.UserName == user.UserName)
                        user.UserName = model.UserName;
                    else
                    {
                        loginUpdate = true;
                        user.UserName = model.UserName;
                    }
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.Age = model.Age;
                    if (model.Email == user.Email)
                        user.Email = model.Email;
                    else
                    {
                        user.Email = model.Email;
                        user.EmailConfirmed = false;
                        var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        IdentityMessage message = new IdentityMessage();
                        message.Subject = "Confirm your account";
                        message.Body = "Please confirm your account by clicking this link: <a href=\"" + callbackUrl + "\">Confirm account</a>";
                        message.Destination = user.Email;
                        await UserManager.EmailService.SendAsync(message);
                        emailUpdate = true;
                    }
                    IdentityResult result = UserManager.Update(user);
                    if (result.Succeeded)
                    {
                        if (emailUpdate)
                        {
                            return Json(new { status = "Redirect", data = new UserProfile(user) }, JsonRequestBehavior.AllowGet);
                        }
                        if (loginUpdate)
                        {
                            ClaimsIdentity ident = await UserManager.CreateIdentityAsync(user,
DefaultAuthenticationTypes.ApplicationCookie);

                            AuthManager.SignOut();
                            AuthManager.SignIn(new AuthenticationProperties
                            {
                                IsPersistent = false
                            }, ident);
                        }
                        return Json(new UserProfile(user), JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new UserProfile(user), JsonRequestBehavior.AllowGet);
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult UserLogin()
        {
            User _user = UserManager.FindByName(User.Identity.Name);
            UserProfile user = new UserProfile(_user);
            var roles = UserManager.GetRoles(user.Id);
            foreach (var role in roles)
            {
                if (UserManager.IsInRole(user.Id, role))
                    user.RoleName = role;
            }
            return PartialView("../PartialView/_UserLoginPartial", model: user);
        }
        public PartialViewResult UserEmailConfirm()
        {
            bool emailConfirmed = false;
            User _user = UserManager.FindByName(User.Identity.Name);
            emailConfirmed = _user.EmailConfirmed;
            return PartialView("../PartialView/_UserEmailConfirmPartial", model: emailConfirmed);
        }
        private UserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
        }
        private RoleManager RoleManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<RoleManager>();
            }
        }
        private IAuthenticationManager AuthManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }
}