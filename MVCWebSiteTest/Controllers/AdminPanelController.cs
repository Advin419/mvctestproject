﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using DataAccess.Repository;
using Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MVCWebSiteTest.Models;

namespace MVCWebSiteTest.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminPanelController : Controller
    {
        private static string roleNameAdmin = "Admin";
        private static string roleNameUser = "User";
        private class Result<T>
        {
            public List<T> data { get; set; }
            public int itemsCount { get; set; }
        }

        public ActionResult Index(string page = null)
        {
            return View();
        }

        public JsonResult GetUsers(int pageSize, int pageIndex, string sortField = "UserName", string sortOrder = "asc")
        {
            List<UserProfile> users = new List<UserProfile>();
            foreach(var u in UserManager.Users.Cast<User>().ToList())
            {
                UserProfile user = new UserProfile(u);
                user.IsAdmin = UserManager.IsInRole(user.Id, roleNameAdmin);
                users.Add(user);
            }
            Result<UserProfile> result = new Result<UserProfile>();
            if (sortOrder == "desc")
            {
                result.data = users.OrderByDescending(x =>
                {
                    var prop = x.GetType().GetProperty(sortField);
                    return prop.GetValue(x);
                }).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                result.data = users.OrderBy(x =>
                {
                    var prop = x.GetType().GetProperty(sortField);
                    return prop.GetValue(x);
                }).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            }
            result.itemsCount = users.Count;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> UpdateUser(EditViewModel model)
        {
            User user = await UserManager.FindByIdAsync(model.Id);
            UserManager.RemoveFromRole(user.Id, roleNameUser);
            UserManager.RemoveFromRole(user.Id, roleNameAdmin);
            if (user != null)
            {
                if (model.UserName != string.Empty || model.UserName != null)
                    user.UserName = model.UserName;
                if (model.FirstName != string.Empty || model.FirstName != null)
                    user.FirstName = model.FirstName;
                if(model.LastName != string.Empty || model.LastName != null)
                    user.LastName = model.LastName;
                if(model.Age != 0)
                    user.Age = model.Age;
                if(model.Email != string.Empty || model.Email != null)
                    user.Email = model.Email;
                if (model.RegDate != null)
                    user.RegDate = model.RegDate;
                if (model.StudyDate != null)
                    user.StudyDate = model.StudyDate;
                if (model.IsAdmin)
                {
                    if (!UserManager.IsInRole(user.Id, roleNameAdmin))
                    {
                        UserManager.AddToRole(user.Id, roleNameAdmin);
                    }
                }
                else
                {
                    if (!UserManager.IsInRole(user.Id, roleNameUser))
                    {
                        UserManager.AddToRole(user.Id, roleNameUser);
                    }
                }
                IdentityResult result = UserManager.Update(user);
                if (result.Succeeded)
                {
                    return Json(new UserProfile(user), JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new UserProfile(user, model.IsAdmin), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteUser(string Id)
        {
            List<UserProfile> users = new List<UserProfile>();
            foreach (var u in UserManager.Users.Cast<User>().ToList())
            {
                UserProfile _user = new UserProfile(u);
                users.Add(_user);
            }
            User user = UserManager.FindById(Id);
            if (user != null)
            {
                IdentityResult result = UserManager.Delete(user);
                if (result.Succeeded)
                {
                    users = new List<UserProfile>();
                    foreach (var u in UserManager.Users.Cast<User>().ToList())
                    {
                        UserProfile _user = new UserProfile(u);
                        users.Add(_user);
                    }
                    return Json(users, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RoleSettings()
        {
            return View();
        }

        public JsonResult GetUserRoles()
        {
            List<UserRoles> userRoles = RoleManager.Roles.ToList();
            Result<UserRoles> result = new Result<UserRoles>();
            result.data = userRoles;
            result.itemsCount = userRoles.Count;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> CreateRole([Required]string roleName)
        {
            List<UserRoles> userRoles = RoleManager.Roles.ToList();
            if (ModelState.IsValid)
            {
                IdentityResult result
                    = await RoleManager.CreateAsync(new UserRoles(roleName));

                if (result.Succeeded)
                {
                    userRoles = RoleManager.Roles.ToList();
                    return Json(userRoles, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    AddErrorsFromResult(result);
                }
            }
            return Json(userRoles, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> DeleteRole(string Id)
        {
            List<UserRoles> userRoles = RoleManager.Roles.ToList();
            UserRoles role = await RoleManager.FindByIdAsync(Id);
            if (role != null)
            {
                IdentityResult result = await RoleManager.DeleteAsync(role);
                if (result.Succeeded)
                {
                    userRoles = RoleManager.Roles.ToList();
                    return Json(userRoles, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    AddErrorsFromResult(result);
                }
            }
            return Json(userRoles, JsonRequestBehavior.AllowGet);
        }


        private void AddErrorsFromResult(IdentityResult result)
        {
            foreach (string error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
        private UserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
        }
        private RoleManager RoleManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<RoleManager>();
            }
        }


    }
}