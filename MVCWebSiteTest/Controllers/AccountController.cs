﻿using Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using DataAccess.Repository;
using MVCWebSiteTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using System.Security.Claims;

namespace MVCWebSiteTest.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private static string roleNameUser = "User";
        private static string roleNameAdmin = "Admin";
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (returnUrl == string.Empty || returnUrl == null)
            {
                returnUrl = Url.Action("Index", "Home");
            }
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            User user = await UserManager.FindAsync(model.Login, model.Password);

            if (user == null)
            {
                ModelState.AddModelError("", "Некорректное имя или пароль.");
            }
            else
            {
                if (user.EmailConfirmed == true)
                {
                    await Login(user, model.RememberMe);
                    return Redirect(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", "Не подтвержден email.");
                }
            }

            return View(model);
        }
        [AllowAnonymous]
        public ActionResult FacebookLogin(string returnUrl)
        {
            var properties = new AuthenticationProperties
            {
                RedirectUri = Url.Action("FacebookLoginCallback",
                    new { returnUrl = returnUrl })
            };

            HttpContext.GetOwinContext().Authentication.Challenge(properties, "Facebook");
            return new HttpUnauthorizedResult();
        }

        [AllowAnonymous]
        public async Task<ActionResult> FacebookLoginCallback(string returnUrl)
        {
            ExternalLoginInfo loginInfo = await AuthManager.GetExternalLoginInfoAsync();
            User user = await UserManager.FindAsync(loginInfo.Login);

            if (user == null)
            {

                user = new User
                {
                    Email = loginInfo.Email,
                    UserName = loginInfo.DefaultUserName,
                    FirstName = loginInfo.ExternalIdentity.Name.Substring(0, loginInfo.ExternalIdentity.Name.LastIndexOf(' ') + 1) ?? "First Name",
                    LastName = loginInfo.ExternalIdentity.Name.Substring(loginInfo.ExternalIdentity.Name.IndexOf(' ')) ?? "Last Name",
                    Age = 0,
                    RegDate = DateTime.Now,
                    StudyDate = DateTime.Now
            };

                IdentityResult result = await UserManager.CreateAsync(user);
                if (!result.Succeeded)
                {
                    foreach(var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.ToString());
                    }
                    return View();
                }
                else
                {
                    if (!UserManager.IsInRole(user.Id, roleNameUser) && !UserManager.IsInRole(user.Id, roleNameAdmin))
                    {
                        UserManager.AddToRole(user.Id, roleNameUser);
                    }
                    result = await UserManager.AddLoginAsync(user.Id, loginInfo.Login);
                    if (!result.Succeeded)
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError("", error.ToString());
                        }
                        return View();
                    }
                }
            }

            ClaimsIdentity ident = await UserManager.CreateIdentityAsync(user,
                DefaultAuthenticationTypes.ApplicationCookie);

            ident.AddClaims(loginInfo.ExternalIdentity.Claims);

            AuthManager.SignIn(new AuthenticationProperties
            {
                IsPersistent = false
            }, ident);

            return Redirect(returnUrl ?? Url.Action("Index", "Home"));
        }
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = new User { UserName = model.Login, Email = model.Email, FirstName = model.FirstName, LastName = model.LastName, Age = model.Age, RegDate = DateTime.Now, StudyDate = DateTime.Now };
                IdentityResult result =
                    await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    IdentityMessage message = new IdentityMessage();
                    message.Subject = "Confirm your account";
                    message.Body = "Please confirm your account by clicking this link: <a href=\"" + callbackUrl + "\">Confirm account</a>";
                    message.Destination = user.Email;
                    await UserManager.EmailService.SendAsync(message);
                    if (!UserManager.IsInRole(user.Id, roleNameUser) && !UserManager.IsInRole(user.Id, roleNameAdmin))
                    {
                        UserManager.AddToRole(user.Id, roleNameUser);
                    }
                    return View("DisplayEmail");
                }
                else
                {
                    AddErrorsFromResult(result);
                }
            }
            return View(model);
        }
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            if (result.Succeeded)
            {
                User user = await UserManager.FindByIdAsync(userId);
                await Login(user, true);
                return Redirect(Url.Action("Index", "Home"));
            }
            ModelState.AddModelError("", result.Errors.First().ToString());
            return View("Login");
        }
        public ActionResult SignOut()
        {
            AuthManager.SignOut();
            return RedirectToAction("Login");
        }
        [AllowAnonymous]
        public ActionResult DisplayEmail()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult ForgotPassword(string returnUrl)
        {
            if (returnUrl == string.Empty || returnUrl == null)
            {
                returnUrl = Url.Action("Index", "Home");
            }
            ViewBag.returnUrl = returnUrl;
            return View();
        }
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    return View("ForgotPasswordConfirmation");
                }

                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account",
            new { UserId = user.Id, code = code }, protocol: Request.Url.Scheme);
                await UserManager.SendEmailAsync(user.Id, "Reset Password",
            "Please reset your password by clicking here: <a href=\"" + callbackUrl + "\">Reset Password</a>");
                return View("ForgotPasswordConfirmation");
            }
            return View(model);
        }
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.UserId == null || model.Code == null)
                {
                    return View("Error");
                }
                var result = await UserManager.ResetPasswordAsync(model.UserId, model.Code, model.Password);
                if (result.Succeeded)
                {
                    User user = await UserManager.FindByIdAsync(model.UserId);
                    await Login(user, true);
                    return Redirect(Url.Action("Index", "Home"));
                }
                ModelState.AddModelError("", result.Errors.First().ToString());
                return View("Login");
            }
            return View(model: model);
        }
        private async Task<bool> Login(User user, bool RememberMe)
        {
            try
            {
                ClaimsIdentity ident = await UserManager.CreateIdentityAsync(user,
        DefaultAuthenticationTypes.ApplicationCookie);

                AuthManager.SignOut();
                AuthManager.SignIn(new AuthenticationProperties
                {
                    IsPersistent = RememberMe
                }, ident);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private void AddErrorsFromResult(IdentityResult result)
        {
            foreach (string error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private IAuthenticationManager AuthManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private UserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<UserManager>();
            }
        }
    }
}