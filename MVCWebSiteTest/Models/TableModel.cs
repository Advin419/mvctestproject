﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

//namespace MVCWebSiteTest.Models
//{
//    public class TableModel<T>
//    {
//        private readonly int itemsPerPage;
//        private readonly int currentPage;
//        private readonly int totalPages;
//        private readonly int totalItemsCount;

//        private readonly IEnumerable<T> result;
//        //private readonly string[] cachedIds; // ids for searched items (use for paging)

//        public TableModel(IEnumerable<T> result, int totalItemsCount = 0, int currentPage = 1, int itemsPerPage = 6)
//        {
//            this.result = result;
//            this.totalItemsCount = totalItemsCount;
//            this.itemsPerPage = itemsPerPage > 0 ? itemsPerPage : 1;
//            this.currentPage = currentPage > 0 ? currentPage : 1;

//            this.totalPages = (this.totalItemsCount % this.itemsPerPage) == 0 ? this.totalItemsCount / this.itemsPerPage : (this.totalItemsCount / this.itemsPerPage) + 1;
//        }

//        /// <summary>
//        /// Get number of selected page
//        /// </summary>
//        public int CurrentPage { get { return currentPage; } }

//        /// <summary>
//        /// Get number of total pages
//        /// </summary>
//        public int TotalPages { get { return totalPages; } }

//        /// <summary>
//        /// Get number of searched items that have been found
//        /// </summary>
//        public int TotalItemsCount { get { return totalItemsCount; } }

//        /// <summary>
//        /// Get number of searched items per one page
//        /// </summary>
//        public int ItemsPerPage { get { return itemsPerPage; } }

//        /// <summary>
//        /// Gets list of searched items
//        /// </summary>
//        public IEnumerable<T> Result { get { return result; } }

//        /// <summary>
//        /// Get array of product IDs that can be cached 
//        /// </summary>
//        //public string[] CachedProductIds { get { return cachedIds; } }
//    }
//}