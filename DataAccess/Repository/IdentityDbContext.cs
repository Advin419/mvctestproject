﻿using Entities;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System;

namespace DataAccess.Repository
{
    public class IdentityDbContext : IdentityDbContext<User>
    {
        public IdentityDbContext() : base("name=IdentityDb") { }

        static IdentityDbContext()
        {
            Database.SetInitializer<IdentityDbContext>(new IdentityDbInit());
        }

        public static IdentityDbContext Create()
        {
            return new IdentityDbContext();
        }
    }

    public class IdentityDbInit : DropCreateDatabaseIfModelChanges<IdentityDbContext>
    {
        protected override void Seed(IdentityDbContext context)
        {
            PerformInitialSetup(context);
            base.Seed(context);
        }
        public void PerformInitialSetup(IdentityDbContext context)
        {
            UserManager userMgr = new UserManager(new UserStore<User>(context));
            RoleManager roleMgr = new RoleManager(new RoleStore<UserRoles>(context));
            User userProfile = new User { FirstName = "Tolia", LastName = "Melnyk", UserName = "Admin", Email = "advin419@gmail.com", Age = 22, RegDate = DateTime.Now, StudyDate = DateTime.Now, EmailConfirmed=true };
            string roleNameAdmin = "Admin";
            string roleNameUser = "User";
            string password = "Aa1234";

            if (!roleMgr.RoleExists(roleNameAdmin))
            {
                roleMgr.Create(new UserRoles(roleNameAdmin));
            }
            if (!roleMgr.RoleExists(roleNameUser))
            {
                roleMgr.Create(new UserRoles(roleNameUser));
            }
            User user = userMgr.FindByName(userProfile.UserName);
            if (user == null)
            {
                user = userProfile;
                userMgr.Create(user,
                    password);
                user = userMgr.FindByName(userProfile.UserName);
            }

            if (!userMgr.IsInRole(user.Id, roleNameAdmin))
            {
                userMgr.AddToRole(user.Id, roleNameAdmin);
            }

        }
    }
}
