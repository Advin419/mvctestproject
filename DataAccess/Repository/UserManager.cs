﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.AspNet.Identity.Owin;
using Entities;
using System.Threading.Tasks;
using System.Net.Mail;
using Microsoft.Owin;
using System.Net;

namespace DataAccess.Repository
{
    public class EmailService : IIdentityMessageService
    {
        protected const string SMTP_HOST = "smtp.gmail.com"; // Amazon SES SMTP host name.
        protected const int SMTP_PORT = 587; // We are choosing port 587 because we will use STARTTLS to encrypt the connection.
        public Task SendAsync(IdentityMessage message)
        {
            // настройка логина, пароля отправителя
            var from = "email";
            var pass = "password";

            // адрес и порт smtp-сервера, с которого мы и будем отправлять письмо
            SmtpClient client = new SmtpClient(SMTP_HOST, SMTP_PORT);

            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(from, pass);
            client.EnableSsl = true;

            // создаем письмо: message.Destination - адрес получателя
            var mail = new MailMessage(from, message.Destination);
            mail.Subject = message.Subject;
            mail.Body = message.Body;
            mail.IsBodyHtml = true;

            return client.SendMailAsync(mail);
        }
    }
    public class UserManager : UserManager<User>
    {
        public UserManager(IUserStore<User> store)
            : base(store)
        { }

        public static UserManager Create(IdentityFactoryOptions<UserManager> options,
            IOwinContext context)
        {
            IdentityDbContext db = context.Get<IdentityDbContext>();
            var provider = new DpapiDataProtectionProvider("MyUserProvider");
            UserManager manager = new UserManager(new UserStore<User>(db));
            manager.UserTokenProvider = new DataProtectorTokenProvider<User>(provider.Create("EmailConfirmation"));

            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true
            };

            manager.UserValidator = new UserValidator<User>(manager)
            {
                AllowOnlyAlphanumericUserNames = true,
                RequireUniqueEmail = true
            };

            manager.EmailService = new EmailService();
            //manager.SmsService = new SmsService();

            return manager;
        }
    }
}
