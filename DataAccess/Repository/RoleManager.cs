﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using Entities;

namespace DataAccess.Repository
{
    public class RoleManager : RoleManager<UserRoles>, IDisposable
    {
        public RoleManager(RoleStore<UserRoles> store)
            : base(store)
        { }

        public static RoleManager Create(
            IdentityFactoryOptions<RoleManager> options,
            IOwinContext context)
        {
            return new RoleManager(new
                RoleStore<UserRoles>(context.Get<IdentityDbContext>()));
        }
    }
}