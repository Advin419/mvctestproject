﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Entities
{
    public class UserRoles : IdentityRole
    {
        public UserRoles() : base() { }

        public UserRoles(string name)
            : base(name)
        { }
    }
}