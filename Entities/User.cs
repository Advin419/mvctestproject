﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Entities
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public DateTime RegDate { get; set; }
        public DateTime StudyDate { get; set; }
    }
}
