﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class UserProfile
    {
        public UserProfile() { }
        public UserProfile(User user, bool isAdmin = false)
        {
            Id = user.Id;
            UserName = user.UserName;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Age = user.Age;
            RegDate = user.RegDate.ToShortDateString();
            StudyDate = user.StudyDate.ToShortDateString();
            Email = user.Email;
            IsAdmin = isAdmin;
        }
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string RegDate { get; set; }
        public string StudyDate { get; set; }
        public string RoleName { get; set; }
        public bool IsAdmin { get; set; }
    }
}
